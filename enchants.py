import requests
import json
import sqlite3

connection = sqlite3.connect('enchants.db')

r = requests.get('https://api.nexushub.co/wow-classic/v1/items/nethergarde-keep-alliance/large-prismatic-shard')
r2 = requests.get('https://api.nexushub.co/wow-classic/v1/items/nethergarde-keep-alliance/arcane-dust')
r3 = requests.get('https://api.nexushub.co/wow-classic/v1/items/nethergarde-keep-alliance/greater-planar-essence')
r4 = requests.get('https://api.nexushub.co/wow-classic/v1/items/nethergarde-keep-alliance/void-crystal')

res = json.loads(r.content)
res2 = json.loads(r2.content)
res3 = json.loads(r3.content)
res4 = json.loads(r4.content)

lpsPrice = res["stats"]["current"]["minBuyout"]/10000
arcaneDustPrice = res2["stats"]["current"]["minBuyout"]/10000
gpePrice = res3["stats"]["current"]["minBuyout"]/10000
crystalPrice = res4["stats"]["current"]["minBuyout"]/10000

cursor = connection.cursor()
cursor.execute("SELECT * from Enchants")
test = cursor.fetchall()
connection.close()

enchantList=[]
for ench in test:
    matsList=[]
    x = ench[1].split(",")
    for mats in x:
        matsList.append(mats.split(":"))
    enchantList.append([ench[0],matsList])
    
for e in enchantList:
    price=0
    primals=""
    for m in e[1]:
        if 'Shard' in m[0]:
            if 'Large' in m[0]:
                price = price + (int(m[1]) * lpsPrice)
            else:
                price = price + (int(m[1]) * lpsPrice / 3)
        if 'Essence' in m[0]:
            if 'Greater' in m[0]:
                price = price + (int(m[1]) * gpePrice)
            else:
                price = price + (int(m[1]) * gpePrice / 3)
        if 'Dust' in m[0]:
            price = price + (int(m[1]) * arcaneDustPrice)
        if 'Crystal' in m[0]:
            price = price + (int(m[1]) * crystalPrice)
        if 'Primal' in m[0]:
            primals+=m[1]+" x"+m[0]+" "

    if primals != "":
        e.append(str(round(price/2,1))+"g + "+primals)
    else:
        e.append(str(round(price/2,1))+"g")

for e in enchantList:
    print("{0:40} {1}".format(e[0], e[2]))
