import sqlite3

connection = sqlite3.connect('enchants.db')

cursor = connection.cursor()
cursor.execute('''DROP TABLE IF EXISTS Enchants''')
cursor.execute('''CREATE TABLE IF NOT EXISTS Enchants
              (Name TEXT, Reagents TEXT)''')

cursor.execute("INSERT INTO Enchants VALUES('Enchant Weapon: Major Spellpower','Large Prismatic Shard:8, Greater Planar Essence:8')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Weapon: Major Healing','Large Prismatic Shard:8, Primal Water:8, Primal Life:8')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Weapon: Major Sunfire','Void Crystal:12, Large Prismatic Shard:10, Greater Planar Essence:8, Primal Fire:6, Primal Might:1')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Weapon: Major Spellsurge','Large Prismatic Shard:12, Greater Planar Essence:10, Arcane Dust:20')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Weapon: Major Intellect','Large Prismatic Shard:2, Greater Planar Essence:10')")

cursor.execute("INSERT INTO Enchants VALUES('Enchant Bracer: Superior Healing','Greater Planar Essence:4, Primal Life:4')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Bracer: Spellpower','Large Prismatic Shard:6, Primal Fire:6, Primal Water:6')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Bracer: Fortitude','Large Prismatic Shard:1, Greater Planar Essence:10, Arcane Dust:20')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Bracer: Major Defense','Small Prismatic Shard:2, Arcane Dust:10')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Bracer: Major Intellect','Lesser Planar Essence:3')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Bracer: Brawn','Arcane Dust:6')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Bracer: Assault','Arcane Dust:6')")

cursor.execute("INSERT INTO Enchants VALUES('Enchant Cloak: Dodge','Small Prismatic Shard:3, Greater Planar Essence:3, Primal Earth:8')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Cloak: Major Armor','Arcane Dust:8')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Cloak: Greater Agility','Greater Planar Essence:1, Arcane Dust:4, Primal Air:1')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Cloak: Subtlety','Small Prismatic Shard:4, Greater Planar Essence:2, Primal Shadow:8')")

cursor.execute("INSERT INTO Enchants VALUES('Enchant Chest: Exceptional Stats','Large Prismatic Shard:4, Arcane Dust:4, Greater Planar Essence:4')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Chest: Exceptional Health','Arcane Dust:8, Major Healing Potion:4, Large Brilliant Shard:2')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Chest: Major Spirit','Greater Planar Essence:2')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Chest: Restore Mana Prime','Lesser Planar Essence:2, Arcane Dust:2')")

cursor.execute("INSERT INTO Enchants VALUES('Enchant Gloves: Major Healing','Greater Planar Essence:6, Large Prismatic Shard:6, Primal Life:6')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Gloves: Major Spellpower','Greater Planar Essence:6, Large Prismatic Shard:6, Primal Mana:6')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Gloves: Spell Strike','Greater Planar Essence:8, Arcane Dust:2, Large Prismatic Shard:2')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Gloves: Major Strength','Arcane Dust:12, Greater Planar Essence:1')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Gloves: Assault','Arcane Dust:8')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Gloves: Blasting','Lesser Planar Essence:1, Arcane Dust:4')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Gloves: Threat','Small Prismatic Shard:4, Greater Planar Essence:2, Primal Life:8')")

cursor.execute("INSERT INTO Enchants VALUES('Enchant Shield: Intellect','Greater Planar Essence:4')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Shield: Resilence','Large Prismatic Shard:1, Lesser Planar Essence:4')")
cursor.execute("INSERT INTO Enchants VALUES('Enchant Shield: Major Stamina','Arcane Dust:15')")

cursor.execute("INSERT INTO Enchants VALUES('Enchant Boots: Fortitude','Arcane Dust:12')")

connection.commit()
connection.close()